import numpy as np

# Sigmoid (interesting activation function)
def sigmoid(x):
    return 1/(1+np.exp(-x))

# Backward Sigmoid for backpropagation
def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))

class mlp():
    def __init__(self, layers, W = None, b = None, learning_rate = 0.01):
        self.layers = layers # [len(input), ... len(hidden_layers) ... , len(output)]
        self.W = W
        self.b = b
        self.lr = learning_rate

        # Remove empty layers
        for i in layers:
            if i <= 0:
                self.layers.remove(i)

        if self.W == None:
            self.W = dict()  # Weights
            for i in range(0, len(self.layers)):
                # Random initialisation of Weights
                self.W[i] = np.random.rand(self.layers[i], self.layers[i-1])

        if self.b == None:
            self.b = dict()  # Bias
            for i in range(1, len(self.layers)):
                # Initialisation of bias
                self.b[i] = np.zeros((self.layers[i], 1))

    # Passing through the all network
    def forward(self, X):
        caches = []
        for i in range(1, len(self.layers)):
            r = np.dot(self.W[i], X) + self.b[i]
            X = sigmoid(r)
            caches.append([r, X])
        return X, caches

    # Backward through the network
    # def backward(self, output, expected, caches):
    #     L = len(self.layers)
    #
    #     dX = dict()
    #     dW = dict()
    #     db = dict()
    #
    #     dX[L - 1] = -(np.divide(expected, output) - np.divide(1 - expected, 1 - output))
    #
    #     for i in range(L - 2, -1, -1):
    #         cache = caches[i]  # the caches could be misplaced, to check
    #         X = cache[1]
    #         n = len(X)
    #         W = self.W[i + 1]
    #         b = self.b[i + 1]
    #         dZ = np.squeeze(dX[i+1] * sigmoid_prime(cache[0])
    #         dX[i] = np.dot(np.transpose(W), dZ)
    #         dW[i] = (1/n) * np.dot(dZ, np.transpose(X))
    #         db[i] = (1/n) * np.sum(dZ)
    #
    #     return dX, dW, db

    # Update the weights and the biases with the given derivatives
    # def update(self, dW, db):
    #     for i in range(1, len(self.layers)):
    #         self.W[i] += self.lr * dW[i]
    #         self.b[i] += self.lr * db[i]

# Cost function (cross-entropy cost)
def compute_cost(output, expected):
    n = len(expected)
    cost = -(1/n)*np.sum((expected*np.log(output)+(1-expected)*np.log(1-output)))
    cost = np.squeeze(cost)   # to avoid having a number surrounded by numeral bracket, that will raise dimension problems
    return cost


if __name__ == "__main__":

    #####################################################################
    # First test (same as the single layer perceptron)
    #####################################################################
    W = dict()
    W[1] = np.array([0.020653640140000002, -0.023418117710000002])
    p = mlp([2, 0, 1], W)

    dataset = [[2.7810836,2.550537003,0],
    [1.465489372,2.362125076,0],
    [3.396561688,4.400293529,0],
    [1.38807019,1.850220317,0],
    [3.06407232,3.005305973,0],
    [7.627531214,2.759262235,1],
    [5.332441248,2.088626775,1],
    [6.922596716,1.77106367,1],
    [8.675418651,-0.242068655,1],
    [7.673756466,3.508563011,1]]

    for i in range(len(dataset)):
        Xi = dataset[i][:2]
        ri = dataset[i][2]
        r, cache = p.forward(Xi)
        res = [r, ri]
        print(res)
        # p.backward(r, ri, cache)


    #####################################################################
    # second test with more data and a training set and prediction set
    #####################################################################
    import random
    dataset = list()
    for i in range(10000):
        f1 = random.uniform(-5.0, 5.0)
        f2 = random.uniform(-5.0, 5.0)
        # We generate points between -5 and 5 (on 2 dimensions) and put them in a class based on their position realatively to a line (here 2x + 1)
        dataset.append([f1, f2, 1.0 if f2 > 2*f1 + 1 else 0.0])
    # We split our data between 90% for training and 10% for testing
    idx = int(len(dataset) * (0.9))
    trainingset = dataset[:idx]
    predictset = dataset[idx:]
    W = dict()
    W[1] = [-0.13637254097087065, 0.0942655828890947]

    p = mlp([2, 0 , 1], W)

    # Testing
    acc = 0
    for i in range(len(predictset)):
        Xi = dataset[i][:2]
        ri = dataset[i][2]
        r, cache = p.forward(Xi)
        res = [r, ri]
        if (r[0, 0] >= 0.5 and ri == 1) or (r[0, 0] < 0.5 and ri == 0):
            acc += 1
        #print ([prIn, prOut])

    print ("Accuracy :" + str(acc/len(predictset)))
    print ("")
