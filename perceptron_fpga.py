import pickle

from nmigen import *
from nmigen.cli import main

from nmigen.back.pysim import *
from nmigen_boards.icestick import ICEStickPlatform

# python3 -m ML simulate -c 10 -v test.vcd
from perceptron import Perceptron

from settings import *

class Main(Elaboratable):
	def __init__(self, b):

		for n, value in enumerate(b):
			b[n] = int(value*ONE)

		self.b = b
		self.s = [Signal(WIDTH) for _ in enumerate(b[:-1])]
		self.ret = Signal(WIDTH)

		self.real = Signal(WIDTH)

		self.accuracy = Signal()
		self.correct = Signal(WIDTH)
		self.total = Signal(WIDTH)

	def elaborate(self, platform):
		m = Module()

		m.submodules.per = Perceptron(*self.b, lim=10000)
		# or anonymous submodules
		# m.submodules += Mod1(2)

		m.d.comb += [ m.submodules.per[n].eq( signal ) for n, signal in enumerate(self.s) ]
		m.d.comb += self.ret.eq(m.submodules.per.out)


		#[source for creating testbench] http://blog.lambdaconcept.com/doku.php?id=nmigen:nmigen_sim_testbench_sync

		#If we got the value in the corresponding range
		# accuracy <- round(predict) == real
		m.d.comb += self.accuracy.eq( ((self.ret >> OFFSET) + self.ret[15])*ONE == self.real )

		# if accuracy:
		# 	correct += 1
		with m.If(self.accuracy):
			m.d.sync += self.correct.eq( self.correct + ONE )
		#by knowing the total and how many we got correct,
		#it's easy to know the % of what we got right
		m.d.sync += self.total.eq( self.total + ONE )

		#LED
		activity_led = platform.request('led', 0)
		m.d.sync += activity_led.eq(m.submodules.per.out[16])

		return m

if __name__ == "__main__":
	#b is the liniar coeficients used on the linear regression and will be passed to the perceptron
	b = [2.32831355e-02, -1.07644595e+00, -1.44067150e-01,  6.57402064e-03, -1.82427840e+00,  3.42047216e-03, -3.42657365e-03, -1.49541411e+01, -2.91748283e-01,  8.72400085e-01,  2.77567469e-01, 18.713908307334766]
	top = Main(b)
	ICEStickPlatform().build(top, do_program=True)
