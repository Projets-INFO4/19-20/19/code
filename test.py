from nmigen import *
from nmigen.cli import main

# python3 test.py simulate -c 10 -v test.vcd

class Mod1(Elaboratable):
    def __init__(self, width):
        self.s1 = Signal(width)
        self.s2 = Signal(width)
        self.s3 = Signal(width)

    def elaborate(self, platform):
        m = Module() #all operation on signals must be done in a module

        #s1 = Signal()
        #r1 = Record([("r", 1), ("g", 1), ("b", 1)])

        m.d.comb += self.s2.eq(self.s3) #s2 change when s3 change
        m.d.sync += self.s3.eq(~self.s3)  #s3 change on every clock tick

        # equaivalent to m.d.comb += s1.eq(s3)
        #with m.If(s3):
        #    m.d.comb += s1.eq(1)
        #with m.Else():
        #    m.d.comb += s1.eq(0)

        return m

class Mod2(Elaboratable):
    def __init__(self, width):
        self.s1 = Signal(width)
        self.s2 = Signal(width)
        self.s3 = Signal(width)

        self.mod1 = Mod1(width)

    def elaborate(self, platform):
        m = Module()

        m.submodules.mod1 = self.mod1
        # or anonymous submodules
        # m.submodules += Mod1(2)

        m.d.comb += [
            self.s1.eq(self.mod1.s1),
            self.s2.eq(self.mod1.s2),
            self.s3.eq(self.mod1.s3),
        ]

        return m

if __name__ == "__main__":
    top = Mod2(2)
    main(top, ports=[top.mod1])
