from sklearn.datasets import fetch_openml
import matplotlib
import matplotlib.pyplot as plt

mnist = fetch_openml('mnist_784')

# Load the datasets
data, label = mnist['data'], mnist['target']

# Split in training and test set
data_train, data_test = data[:60000], data[60000:]
label_train, label_test = label[:60000], label[60000:]
