import serial
import io
import serial.tools.list_ports

ser = serial.Serial()
ser.port = '/dev/ttyUSB1'
ser.timeout = 1
ser.open()
print(ser.name)

ser.write(b"hello\n")
ser.flush()

hello = ser.readline()

print(hello.decode("Ascii"))

ser.close()
