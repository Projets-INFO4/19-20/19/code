import itertools

from nmigen import *
from nmigen.back.pysim import *
from nmigen.build import *
from nmigen_boards.icestick import ICEStickPlatform
from perceptron import Perceptron
from csv import reader

from uart import UART

import serial

from layer import Layer
from settings import *

class Test(Elaboratable):
    def __init__(self):
        # b is the liniar coeficients used on the linear regression and will be passed to the perceptron
        b = [-0.17300000000000004, -0.15705000000000005, 0.07119999999999994, -0.05700000000000004, -0.021080000000000012, -0.3550000000000006, -0.94, -0.09043039999999998, -0.32160000000000016, 0.030600000000000016, 0.1430000000000002, 0]
        for n, value in enumerate(b):
          b[n] = int(value*ONE)
        self.b = b
        self.p = Perceptron(self.b)

    def elaborate(self, platform):

        uart_pins = platform.request('uart')
        uart = UART(divisor=5)

        m = Module()

        m.submodules.per = self.p
        m.submodules.uart = uart

        m.d.comb += uart.tx_o.eq(uart_pins.tx)
        m.d.comb += uart.rx_i.eq(uart_pins.rx)

        ###########################################################
        with m.If(uart.rx_rdy):
            for p in self.p.ports:
                m.d.sync += p.eq(uart.rx_data)
            m.d.sync += uart.tx_data.eq(m.submodules.per.out)
            m.d.comb += uart.tx_rdy.eq(1)
            Tick()
            m.d.comb += uart.tx_rdy.eq(0)
        ###########################################################

        return m


if __name__ == "__main__":
    ICEStickPlatform().build(Test(), do_program=True)
