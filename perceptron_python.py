from csv import reader

# A perceptron with training and prediction methods
class Perceptron():

    # First implementation of a perceptron in python (without nmigen)
    # It implements the training (wich will modify the weights) and the prediction (which will not)
    def __init__(self, weights, learning_rate = 0.01):
        self.bias = 0.0
        self.weights = weights
        self.learning_rate = learning_rate

    # It will return a prediction based on data provided and the weights of the perceptron
    # param : input -> the data to be tested
    # return : the result of the prediction (0 or 1)
    def predict(self, input):
        activation = -self.bias
        for i in range(len(self.weights)):
            activation += self.weights[i] * input[i]
        prediction = 1 if activation >= 0.0 else 0
        return prediction

    # It will return a prediction based on data provided and the weights of the perceptron.
    # It will also adjust the weights depending on the comparison between the expected and the effective output.
    # param :   expected -> the expected result of a prediction using these data
    #           input -> the data to be tested
    # return : the result of the prediction (0 or 1)
    def train(self, expected, input):
        predicted = self.predict(input)
        if (predicted != expected):
            for i in range(len(self.weights)):
                self.weights[i] = self.weights[i] + self.learning_rate * (expected - predicted) * input[i]
        return predicted

if __name__ == "__main__":

    #####################################################################
    # first test with few data
    #####################################################################
    # Each data is put with the expected result of the prediction at the end of the array
    dataset = [[2.7810836,2.550537003,0],
	[1.465489372,2.362125076,0],
	[3.396561688,4.400293529,0],
	[1.38807019,1.850220317,0],
	[3.06407232,3.005305973,0],
	[7.627531214,2.759262235,1],
	[5.332441248,2.088626775,1],
	[6.922596716,1.77106367,1],
	[8.675418651,-0.242068655,1],
	[7.673756466,3.508563011,1]]
    # Weights initialisation to 0
    weights = [0.0 for _ in range(2)]

    p = Perceptron(weights)
    for k in range(3):
        acc = 0.0
        print ("apres",k,"entrainements")
        print ("Weights :", p.weights)

        # Testing
        for i in range(len(dataset)):
            prIn = dataset[i][len(weights)]
            prOut = p.predict(dataset[i])
            if (prIn == prOut):
                acc += 1
            print ([prIn, prOut])

        # Training
        for i in range(len(dataset)):
            p.train(dataset[i][len(weights)], dataset[i])

        print ("Accuracy :" + str(acc/len(dataset)))
        print ("")
    # Caution !! Might cause overfitting

    #####################################################################
    # second test with more data and a training set and prediction set
    #####################################################################
    import random
    dataset = list()
    for i in range(10000):
        f1 = random.uniform(-5.0, 5.0)
        f2 = random.uniform(-5.0, 5.0)
        # We generate points between -5 and 5 (on 2 dimensions) and put them in a class based on their position realatively to a line (here 2x + 1)
        dataset.append([f1, f2, 1.0 if f2 > 2*f1 + 1 else 0.0])
    # We split our data between 90% for training and 10% for testing
    idx = int(len(dataset) * (0.9))
    trainingset = dataset[:idx]
    predictset = dataset[idx:]
    weights = [0.0 for _ in range(2)]

    p = Perceptron(weights)
    for k in range(2):
        acc = 0.0
        print ("apres",k,"entrainements")
        print ("Weights :", p.weights)

        # Testing
        for i in range(len(predictset)):
            prIn = predictset[i][len(weights)]
            prOut = p.predict(predictset[i])
            if (prIn == prOut):
                acc += 1
            #print ([prIn, prOut])

        # Training
        for i in range(len(trainingset)):
            p.train(trainingset[i][len(weights)], trainingset[i])

        print ("Accuracy :" + str(acc/len(predictset)))
        print ("")

    #####################################################################
    # third test with a csv file
    #####################################################################
    dataset = list()
    with open("data/winequality-red.csv", 'r') as file:
        csv_reader = reader(file)
        for row in csv_reader:
            if not row:
                continue
            line = [float((row[0].split(";"))[i]) for i in range(12)]
            # We will separate the wines between those under the quality 6 and those above
            line[11] = 1.0 if line[11] > 6 else 0.0
            dataset.append(line)
    idx = int(len(dataset) * (0.9))
    trainingset = dataset[:idx]
    predictset = dataset[idx:]
    weights = [0.0 for _ in range(11)]

    p = Perceptron(weights)
    for k in range(2):
        acc = 0.0
        print ("apres",k,"entrainements")
        print ("Weights :", p.weights)

        # Testing
        for i in range(len(predictset)):
            prIn = predictset[i][len(weights)]
            prOut = p.predict(predictset[i])
            if (prIn == prOut):
                acc += 1
            #print ([prIn, prOut])

        # Training
        for i in range(len(trainingset)):
            p.train(trainingset[i][len(weights)], trainingset[i])

        print ("Accuracy :" + str(acc/len(predictset)))
        print ("")

    #####################################################################
    # test with handwritten digit (recognition of a 2) what can our single layer perceptron do?
    #####################################################################
    import digit

    trainingset = digit.data_train
    trainingset_label_raw = digit.label_train
    trainingset_label = list()
    for i in range(len(trainingset_label_raw)):
        trainingset_label.append(1.0 if (int(trainingset_label_raw[i]) == 2) else 0.0)

    predictset = digit.data_test
    predictset_label_raw = digit.label_test
    predictset_label = list()
    nb_of_two = 0.0
    for i in range(len(predictset_label_raw)):
        k = 0.0
        if (int(predictset_label_raw[i]) == 2):
            k = 1.0
            nb_of_two += 1
        predictset_label.append(k)
    print ("Number of 2 :" + str(nb_of_two))

    weights = [0.0 for _ in range(28*28)] # Size of each images

    p = Perceptron(weights)
    for k in range(2):
        acc = 0.0
        acc_on_two = 0.0
        false_positive = 0.0
        print ("apres",k,"entrainements")
        #print ("Weights :", p.weights)

        # Testing
        for i in range(len(predictset)):
            prIn = predictset_label[i]
            prOut = p.predict(predictset[i])
            if (prIn == 0.0) and (prOut == 1.0):
                false_positive += 1
            if (prIn == prOut):
                if (prIn == 1):
                    acc_on_two += 1
                acc += 1
            #print ([prIn, prOut])

        # This accuracy may make no sense as a perceptron that would always predict 0 will have around 90% of Accuracy,
        # It needs to be compared to the number of 2 in the dataset.
        print ("Accuracy :" + str(acc/len(predictset))) # ~ 0.95
        print ("Accuracy on 2 :" + str(acc_on_two/nb_of_two)) # ~ 0.5
        print ("False positive :" + str(false_positive/len(predictset))) # ~ 0.001
        # We can deduce from these numbers that our perceptron is too restrictive.
        # It can easily say that a number is not 2 but is not particularly efficient to recognise a 2
        print ("")

        # Training
        for i in range(len(trainingset)):
            p.train(trainingset_label[i], trainingset[i])
